<?php
const API_URL = 'https://api.exchangeratesapi.io/latest';

class Currency {
    private $to = '';

    public function setToCurrency($value) {
        $this->to = $value;
    }
    public function calculate($amount){
        $conversion = $amount * $this->getExchangeRate();
        return $conversion;
    }
    private function getExchangeRate(){
        $exchangeRate = $this->getRates()->{$this->to};
        return floatval($exchangeRate);
    }

    private function getRates(){
        if(!$this->rates){
            $json = file_get_contents(API_URL);
            if ($json) {
                $data = json_decode($json);
                return $data->rates;
            }
            else{
                throw new Exception('Error retrieving exchange rates through API.');
            }
        }
    }
}
?>