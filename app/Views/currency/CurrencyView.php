<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Currency calculator</title>
</head>

<body>
    <div class="container">
        <h1>Enter amount in dollars</h1>
        <form action="currency/calculate" method="post">
            <p>Dollars:
                <input type="text" name="dollars" />
                <input type="submit" />
            </p>
        </form>
    </div>
</body>

</html>