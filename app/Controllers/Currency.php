<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\CurrencyModel;

    class Currency extends BaseController {             //Tämä toimii kontrollerina

        public function index() {                       // tämä on kontrollerin pääsivu
            return view('currency/CurrencyView');                                            // tällä ladataan näkymä käyttäjälle Views kansiosta
        }
        public function calculate(){                                                                 
                //ladataan malli
            $currencyModel = new CurrencyModel();
                                                            //välitetään raha summa
            //Luetaan käyttäjän syöttämät dollarit
            $dollar = $this->request->getVar("dollars");
            // välitetään rahasumma mallille
            $euro = $currencyModel->calculateEuros($dollar);
            //mutoillaan parametrit
            $data['title'] = "Dollars to Euros Conversion";
            $data['heading'] = "Result for conversion";
            $data['dollarit'] = $dollar;
            $data['eurot'] = $euro;
            //ladataan näkymä joka näyttää tulokset
            echo view('currency/showConversion', $data);
        }
        public function kolmas($nimi){                  // nämä ovat alasivuja Currency sivustolle
            echo "<h1>terve $nimi</h1>";
        }
    }
    ?>
