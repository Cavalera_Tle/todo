<?php namespace App\Controllers;

const REGISTER_TITLE = 'Todo - Register';

class Register extends BaseController{
	
    public function register(){
		$data['title'] = REGISTER_TITLE;
		echo view('templates/header', $data);
		echo view('Login/register', $data);
		echo view('templates/footer', $data);
	}

}
?>