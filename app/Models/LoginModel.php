<?php namespace App\Models;

use CodeIgniter\Model;

class LoginModel extends Model {

    protected $table = 'user';

    protected $allowedFields = ['username', 'password', 'firstname', 'lastname'];

    public function check($username, $password){
        $this->where('username', $username);
        $query = $this->get();
        //print $this->getLastQuery(); // this might be used for debuggin purposition
        $row = $query->getRow();
        if ($row) {     //Check if SQL returned a row.
            if (password_verify($password, $row->password)) {       // verify password
                return $row;
            }
        }
        return null;                //Null will be returned, if there is no user with given username
    }    
    }