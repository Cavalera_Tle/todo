DROP DATABASE if EXISTS todo;

CREATE DATABASE todo;

use todo;

CREATE TABLE user (
    id int PRIMARY KEY auto_increment,
    username VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    firstname VARCHAR(100),
    lastname VARCHAR(100)
);

CREATE TABLE task (
    id int PRIMARY KEY auto_increment,
    title VARCHAR(255) NOT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    description text,
    user_id int not null,
    INDEX (user_id),
    FOREIGN KEY (user_id) REFERENCES user(id)
    on DELETE RESTRICT
)
                          